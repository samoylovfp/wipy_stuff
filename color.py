#!python3
import socket
import sys
from const import WIPY
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
def _send(index, r, g, b):
    s.sendto(b"col" + bytes([index, r, g, b]), (WIPY, 8668))

def red():
    _send(0, 0xFF, 0x00, 0x00)
    _send(1, 0xFF, 0x20, 0x00)
    _send(2, 0xFF, 0x40, 0x00)


def blue():
    _send(0, 0x00, 0x00, 0xFF)
    _send(1, 0x80, 0x00, 0xFF)
    _send(2, 0x00, 0x80, 0xFF)

def green_yellow():
    _send(0, 0x00, 0xFF, 0x00)
    _send(1, 0xFF, 0xFF, 0x00)
    _send(2, 0xFF, 0x80, 0x00)

def rainbow():
    _send(0, 0x00, 0xFF, 0x00)
    _send(1, 0xFF, 0x00, 0x00)
    _send(2, 0x00, 0x00, 0xFF)


if len(sys.argv) < 2 or sys.argv[1] not in locals():
    print([k for k,v in globals().items() if callable(v) and not v.startswith('_')])
else:
    locals()[sys.argv[1]]()
