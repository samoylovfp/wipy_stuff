from const import WIPY
from ftplib import FTP


def upload():
    f = FTP(WIPY, user='micro', passwd='python')
    print(f.set_pasv(True))
    for fname in ['waves.py', 'neo.py', 'main.py']:
        print(f.storbinary(f"STOR /flash/{fname}", open(fname, 'rb')))
    print(f.quit())
