import neo
import time
import socket

SCALE = 100
COLOR_SCALE = 100
MARGIN = 10
LENGTH = 60

MIN_VEL = 10
MAX_VEL = 20
TOTAL_WAVES = 30

LOW_END = -MARGIN * SCALE
HIGH_END = (LENGTH + MARGIN) * SCALE

TOP_SCALE = SCALE * COLOR_SCALE

ws = neo.WS2812(LENGTH)
# ws.set_brightness(255)

cs = None

class Wave:
    color_tints = [
        [255, 0, 0],
        [255, 20, 0],
        [255, 0, 50]
    ]
    brightness = 20
    colors = [
        [0, 0, 0]
        for c in color_tints
    ]

    @classmethod
    def recalculate_color(cls):
        for i in range(len(cls.color_tints)):
            for c in range(3):
                cls.colors[i][c] = cls.color_tints[i][c] * cls.brightness // 255

    def __init__(self):
        # diodes * SCALE
        self.pos = 0
        self.vel = 10
        self.color = [255, 0, 0]

    def reset(self, rand):

        self.pos = (
            (LENGTH + 2 * MARGIN) * (rand % 2) - MARGIN
        ) * SCALE

        self.vel = max(rand % MAX_VEL, MIN_VEL)
        if self.pos > 0:
            self.vel *= -1

        chosen = self.colors[rand % len(self.colors)]
        for i in range(3):
            self.color[i] = chosen[i]

    def advance(self):
        self.pos += self.vel
        if self.pos < LOW_END or self.pos > HIGH_END:
            self.reset(time.ticks_ms())

    def draw(self, d, debug=False):
        pos_i = self.pos // SCALE
        start = max((pos_i - MARGIN), 0)
        end = min((pos_i + MARGIN), LENGTH)
        for i in range(start, end):
            diff = abs(self.pos - (i * SCALE))
            # COLOR_SCALE at the highest, COLOR_SCALE // 2 at the next
            it = TOP_SCALE // max(diff * 2, SCALE)
            if debug:
                print(id(self), "pos", self.pos, "diode", i, "light", it, "color", self.color[0] * it // COLOR_SCALE)
            d[i][0] = min(d[i][0] + (self.color[0] * it // COLOR_SCALE), 255)
            d[i][1] = min(d[i][1] + (self.color[1] * it // COLOR_SCALE), 255)
            d[i][2] = min(d[i][2] + (self.color[2] * it // COLOR_SCALE), 255)


def wave():
    Wave.recalculate_color()
    global cs
    if cs is None:
        cs = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        cs.bind(('0.0.0.0', 8668))
        cs.setblocking(0)
    
    command = bytearray(16)
    display = [[0, 0, 0] for _ in range(LENGTH)]
    waves = [Wave() for _ in range(TOTAL_WAVES)]
    r = time.ticks_ms()
    for w in waves:
        w.reset(r)
        w.pos = r % (HIGH_END - LOW_END) + LOW_END
        r += 1234

    while True:
        read = cs.readinto(command)
        if read is not None and read >= 4:
            if command[:3] == b'bri':
                Wave.brightness = command[3]
            if command[:3] == b'col':
                index = command[3]
                if index < len(Wave.color_tints):
                    Wave.color_tints[index][0] = command[4]
                    Wave.color_tints[index][1] = command[5]
                    Wave.color_tints[index][2] = command[6]
            Wave.recalculate_color()

        for d in display:
            d[0] = 0
            d[1] = 0
            d[2] = 0
        for w in waves:
            w.advance()
            w.draw(display)
        ws.show(display)
        

def test_strip():
    # display = [[i, (i+85) % 255, (i+170) % 255] for i in range(LENGTH)]
    display = [[i, 0, 0] for i in range(LENGTH)]
    while True:
        for cell in display:
            for c in range(1):
                cell[c] = (cell[c] + 1) % 255
        ws.show(display)
