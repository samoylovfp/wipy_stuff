#!python3
import string
import socket
import time

from const import WIPY
from upload import upload

telnet_counter = 0


def bprint(d):
    if isinstance(d, int):
        d = bytes([d])
    if len(d) != 1:
        for c in d:
            bprint(c)
        return

    global telnet_counter

    if d == b'\xff':
        telnet_counter = 3
    if telnet_counter <= 0 and d in string.printable.encode():
        print(d.decode(), end="")
    else:
        telnet_counter -= 1
        print("{:>02X} ".format(ord(d)), end="")


class WipyConsole:
    def __init__(self):
        self.socket = None
        self.connect(WIPY)
        self.tell("Login as: ", "micro")
        self.tell(b"Password: \xFF\xFB\x01\xFF\xFC\x03\xFF\xFB\x22", "python")
        self.tell("succeeded!", "\x03")
        time.sleep(0.5)

    def connect(self, address):
        while self.socket is None:
            s = socket.socket()
            try:
                s.connect((address, 23))
                self.socket = s
            except Exception as e:
                print("Cannot connect", e)
                time.sleep(0.5)

    def wait(self, await_response):
        if not isinstance(await_response, bytes):
            await_response = await_response.encode()
        buffer = bytearray()
        print("waiting for", repr(await_response))
        print("> ", end='')
        while True:
            if b"Invalid" in buffer:
                exit(1)
            response = self.socket.recv(1)
            if not response:
                break
            bprint(response)
            buffer.append(ord(response))
            if await_response in buffer:
                break
        print("")

    def tell(self, after, message=None):
        if message is None:
            message = after
            after = None
        if after is not None:
            self.wait(after)
        message = message.encode() + b'\r\n'
        print("< ", end="")
        bprint(message)
        self.socket.sendall(message)

    def flush(self):
        self.socket.settimeout(0.1)
        print("> ", end="")
        while True:
            try:
                bprint(self.socket.recv(1))
            except socket.timeout:
                self.socket.settimeout(2)
                return

    def restart(self):
        self.flush()
        self.tell("import waves")
        self.tell(">>> ", "waves.wave()")
        time.sleep(1.0)
        self.flush()

    def reboot(self):
        # Ctrl+D
        self.tell(">>> ", '\x04')
        print(self.socket.recv(4096))


upload()
WipyConsole().reboot()
# WipyConsole().restart()
